#!/bin/bash
EXIT_CODE=0;

while getopts :e: flag
do
  case "${flag}" in
    e) ENDPOINT=${OPTARG};;
  esac
done

if [ -z "$ENDPOINT" ]
then
  echo "usage: ./upcheck.sh -e <loadbalancer DNS name>";
  exit 1;
fi

STATUS_CODE=$(curl -w '%{response_code}' -s -o resbody.txt $ENDPOINT)

if [ "$STATUS_CODE" = 200 ]
then
  echo "Service up at endpoint - received status $STATUS_CODE";
else
  echo "Service may not be up at endpoint - received status $STATUS_CODE";
  EXIT_CODE=1;
fi

if grep -q "$PAGE_MESSAGE" resbody.txt
then
  printf 'Service is serving welcome page - received body:\n %s' "$(cat resbody.txt)";
else
  printf 'Service may not be serving expected welcome page - received body\n %s' "$(cat resbody.txt)";
  EXIT_CODE=1;
fi

rm resbody.txt;
exit $EXIT_CODE;
