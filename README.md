# Colin McCoy Lendflow DevOps Assessment Submission

## Viewing the end product
The service is currently up, and serving an html file via nginx 
[here](http://lendf-loadb-1ezoau2wzxfy4-1357885826.us-west-2.elb.amazonaws.com/).

## Requirements
To run locally, you will need the [appropriate version of the Docker Engine](https://docs.docker.com/get-docker/) for your operating system installed on your local machine, and you 
will need for the Docker daemon to be running. The instructions below for running locally also assume that you have a version of Docker later than the one provided in Docker Desktop
3.4.0, if this is not the case, you can use the older `docker-compose` command in lieu of `docker compose`. To use the provided deployment script, you will need to have installed
the [aws-cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) tool. The deploy script accepts a -u flag which takes a profile argument, we recommend
taking advantage of this to designate a profile in `~/.aws/credentials` to use with the aws-cli for your deployment 
[Creating an AWS Credentials file](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html). If you wish to build and deploy an image from the Dockerfile
via the deploy script (-i flag), you must also have the [Amazon ECR Docker Credential Helper](https://github.com/awslabs/amazon-ecr-credential-helper) installed and configured,
or else modify the script to use a different docker registry or credential store. The deploy script also uses the command line utility [jq](https://stedolan.github.io/jq/)
to pretty-print the output of the stack after the deploy has completed. If you don't have this utility installed and don't wish to install it, you can simply delete
the call to jq from the end of the describe-stacks operation on line 51, and allow the script to print the entire output of describe-stacks instead.

## Running locally
Run `docker compose up -d` and navigate to [localhost port 8080](http:127.0.0.1:8080). Run `docker compose down` when finished.

## Deploying to AWS
See the Requirements section above for necessary tooling and configuration. 

From the repository root,

    source .env
    ./deploy.sh -i -u <yourAccountProfile>

With the above arguments, this script will attempt to
  - Create an Amazon ECR repository based on the account, region, and repo name
  specified in `.env`
  - Build and push an image from the included Dockerfile to this repository
  - Create or update a Cloudformation stack containing all of the resources
  necessary for this project (excepting VPC and subnets), using the variables
  set in .env as stack parameters.

The script will attempt to use credentials specified in your `~/.aws/credentials`
file and matching the argument passed via the `-u` flag. Omit this flag if
you wish to have the aws-cli resolve your credentials via a different 
[configuration setting](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html).


The `-i` flag tells the script to build and deploy a docker image from the
provided Dockerfile. If the image has already been deployed or you wish
to use a different image, this flag can be ommitted, however, if you do
wish to use a different image, you should also modify line 42 of the deploy
script to pass a different image identifier to the stack.

### The .env file
The `.env` file is preconfigured with all of the appropriate options for
deploying this project to my personal AWS account with the stack name, project
name, tags, VPC, and subnets I've chosen. Committing an `.env` file is not the
practice I would normally follow, but this one contains no secrets, and is useful
for passing you an appropriate starting configuration under these circumstances. 

Using the AWS access keypair I've
provided, you could theoretically deploy this stack to my account without
any modification of this file. However, this will likely not be a very
interesting test, as I've already deployed this stack and am leaving it
up for demonstration purposes. 

If you wish to see a full deployment using
my account and the credentials I've provided, I recommend putting that
keypair into your `~/.aws/credentials` folder with an appropriate profile
name, and modifying the `STACK_NAME` and `PROJEC_NAME` variables so that a 
new stack is created. Don't forget to source `.env` or open a new shell 
after making your changes. If you wish to deploy the project to a different AWS 
account, modify the `.env` values for `SUBNETS`, `VPC`, `AWS_REGION`, `AWS_ACCOUNT`, 
`S3_DEPLOY_BUCKET`, and `S3_DEPLOY_PREFIX` as appropriate to your account.

If you wish to subscribe to the SNS alarm topic created by this project,
you should also update the `ALARMS_EMAIL` variable.

## Tearing down

    aws cloudformation delete-stack --stack-name <yourStack> --region <yourRegion> --profile <yourProfile>

This command will delete all resources defined by the cloudformation stack.

## Health Checks
A script has been provided to check that the service is running and publicly
accessible. This script can be run with 

    ./upcheck.sh -e <ALBEndpoint>

where ALBEndpoint is the DNS Name of the application load balancer, as provided
in the output of the deploy script. The script will use a simple `curl`
request to hit the endpoint, and check that the service responds with a 200 code,
and that the response body includes our expected html message as configured in the
docker image, printing the results of both checks.

In addition, a CloudWatch alarm has been provisioned in the stack, that will
notify the provisioned SNS topic if the service fails 2 consecutive health checks,
as defined in the load balancer's target group configuration.

## Potential Improvements
This is not an exhaustive list of improvements that could be made to this stack,
but rather the first few improvements that I would make if I were to spend more
time fleshing it out into a production-ready application.

- Add more CloudWatch Alarms, on metrics such as CPUUtilization and MemoryUtilization
on the ECS Cluster, and HTTPCode_ELB_5XX_Count, RejectedConnectionCount,
and TargetResponseTime on the load balancer.
- Register a friendly domain name for the endpoint, and provision a certificate and HTTPS 
listener for this endpoint, with the HTTP endpoint redirecting to HTTPS.
- Create one or more additional SNS topics, for monitoring of thresholds and 
metrics below the critical alarm level.
- Subscribe a Slack channel, PagerDuty service, and/or monitoring dashboard to the
SNS alarms topics, for better alerting and monitoring.
- Expand on the health check script, perhaps having it do a more nuanced status code 
check, for non 200 status codes that don't necessarily indicate a problem with the
service, do a deeper validation of the response body, or set it up to run
on a schedule, e.g. in a lambda function with event bridge.
- Provide a remote configuration source, such as Secrets Manager, DynamoDB, or
Parameter Store, for pulling in the configuration parameters currently set in `.env`
from outside of the codebase.
