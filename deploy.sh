#!/bin/bash
# set -o xtrace

while getopts :iu: flag
do
  case "${flag}" in
    i) DEPLOY_IMAGE=true ;;
    u)
      export AWS_PROFILE=${OPTARG} 
      PROFILE_FLAG="--profile ${AWS_PROFILE}";;
  esac
done

IMAGE_URI="$AWS_ACCOUNT.dkr.ecr.$AWS_REGION.amazonaws.com/$IMAGE_REPO:latest"

if [ "$DEPLOY_IMAGE" = true ];
then
  echo "deploying image"
  aws ecr create-repository \
    --repository-name $IMAGE_REPO \
    --region $AWS_REGION \
    $PROFILE_FLAG

  docker build -t $IMAGE_URI .
  docker push $IMAGE_URI
fi

aws cloudformation package \
  --template-file ./template.yaml \
  --s3-bucket $S3_DEPLOY_BUCKET \
  --s3-prefix $S3_DEPLOY_PREFIX \
  --output-template-file ./cloudformationTemplate.yaml \
  $PROFILE_FLAG

aws cloudformation deploy \
  --template-file ./cloudformationTemplate.yaml \
  --stack-name $STACK_NAME \
  --capabilities CAPABILITY_IAM \
  --parameter-overrides \
    ProjectName=$PROJECT_NAME \
    Vpc=$VPC PublicSubnets=$SUBNETS \
    NginxImage=$IMAGE_URI \
    AlarmsEmail=$ALARMS_EMAIL \
  --tags $STACK_TAGS \
  --region $AWS_REGION \
  $PROFILE_FLAG

ALB_ENDPOINT=$(aws cloudformation describe-stacks \
    --stack-name $STACK_NAME \
    --region $AWS_REGION \
    | jq -r '.Stacks[0].Outputs[0].OutputValue')

printf 'Endpoint (DNS Name) for the application load balancer: %s' $ALB_ENDPOINT
